package me.zhanghai.ohosfastscroll.fragment;


import me.zhanghai.ohosfastscroll.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class FragmentWebView extends Fraction {

    private final String url = "https://www.cnblogs.com/agilezhu/p/6689839.html";

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        //        webView = (WebView) component.findComponentById(ResourceTable.Id_webview);
//        webView.getWebConfig() .setJavaScriptPermit(true);
//        webView.load(url);
        return scatter.parse(ResourceTable.Layout_fragment_five, container, false);
    }
}