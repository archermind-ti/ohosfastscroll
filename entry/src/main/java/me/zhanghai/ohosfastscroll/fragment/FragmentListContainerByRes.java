package me.zhanghai.ohosfastscroll.fragment;


import me.zhanghai.ohos.fastscroll.ListContainerByRes;
import me.zhanghai.ohosfastscroll.ResourceTable;
import me.zhanghai.ohosfastscroll.slice.provider.TestListViewAdapter;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FragmentListContainerByRes extends Fraction {


    private Locale[] mLocales;
    private final List<String> data = new ArrayList<>();
    private final Context context;
    private final int lie;

    public FragmentListContainerByRes(Context context, Locale[] mLocales, List<String> data, int lie) {
        this.mLocales = mLocales == null || mLocales.length == 0 ? new Locale[]{} : mLocales.clone();
        this.data.addAll(data);
        this.lie = lie;
        this.context = context;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fragment_two, container, false);
        init(component);
        return component;
    }

    public void init(Component component) {
        mLocales = Locale.getAvailableLocales();
        for (Locale mLocale : mLocales) {
            data.add(mLocale.toString().charAt(0) + "");
        }
        ListContainerByRes tlv = (ListContainerByRes) component.findComponentById(ResourceTable.Id_tlv);
        //设置滚动条的背景颜色
        tlv.setFastScrollBg(new Color(Color.getIntColor("#BBBBBB")));
        //设置滚动条的图像资源
        tlv.setScrollBarRes(ResourceTable.Graphic_bg_text);
        //设置滚动条的宽高
        tlv.setScrollBarResHW(60, 250);
        //设置文字的背景图片
        tlv.setTextBackGroudRes(ResourceTable.Graphic_left_bg1);
        //设置背景图片的宽高
        tlv.setTextBackGroudShapeWH(200, 200);
        //设置文字颜色
        tlv.setTextColors(Color.RED);
        //设置列数（与listcontainer的列数相同，用于计算高度）
        tlv.setLie(lie);
        //设置显示的文字内容
        tlv.setShowTextList(data);
        //设置listcontainer列数
        TableLayoutManager manager = new TableLayoutManager();
        manager.setColumnCount(lie);
        tlv.setLayoutManager(manager);
        TableLayoutManager c = (TableLayoutManager) tlv.getLayoutManager();
        System.out.println(c.getColumnCount());
        tlv.setItemProvider(new TestListViewAdapter(context, mLocales));
    }
}