package me.zhanghai.ohosfastscroll.fragment;


import me.zhanghai.ohos.fastscroll.ListContainerByShape;
import me.zhanghai.ohosfastscroll.ResourceTable;
import me.zhanghai.ohosfastscroll.slice.provider.TestListViewAdapter;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TableLayoutManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Fragment_ListContanerByShape extends Fraction {
    protected Locale[] mLocales;
    private final List<String> data = new ArrayList<>();
    private final Context context;
    private final int lie;

    public Fragment_ListContanerByShape(Context context, Locale[] mLocales, List<String> data, int lie) {
        this.mLocales = mLocales == null || mLocales.length == 0 ? new Locale[]{} : mLocales.clone();
        this.data.addAll(data);
        this.lie = lie;
        this.context = context;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fragment_one, container, false);
        init(component);
        return component;
    }

    public void init(Component component) {
        ListContainerByShape tlv = (ListContainerByShape) component.findComponentById(ResourceTable.Id_tlv);
        tlv.setRadius(10);//设置圆角弧度
        tlv.setScrollBarWH(50, 200);//设置滚动条宽高
        tlv.setTextBackGroudShapeWH(100, 100);//设置显示文字的背景的宽高
//        tlv.setTextColorops(Color.RED);//设置文字颜色
        tlv.setLie(lie);//设置列
        tlv.setShowTextList(data);
//        tlv.setIsRoundLine(true);
        TableLayoutManager manager = new TableLayoutManager();//设置布局列相关
        manager.setColumnCount(lie);//设置列
        tlv.setLayoutManager(manager);
        TableLayoutManager c = (TableLayoutManager) tlv.getLayoutManager();
        System.out.println(c.getColumnCount());
        tlv.setItemProvider(new TestListViewAdapter(context, mLocales));

    }
}