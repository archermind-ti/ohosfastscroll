package me.zhanghai.ohosfastscroll.fragment;


import me.zhanghai.ohos.fastscroll.ListContainerByRes;
import me.zhanghai.ohosfastscroll.ResourceTable;
import me.zhanghai.ohosfastscroll.slice.provider.TestListViewAdapter;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FragmentGridListByRes extends Fraction {

    private Locale[] mLocales;
    private final List<String> data = new ArrayList<>();
    private final Context context;
    private final int lie;

    public FragmentGridListByRes(Context context, Locale[] mLocales, List<String> data, int lie) {
        this.mLocales = mLocales == null || mLocales.length == 0 ? new Locale[]{} : mLocales.clone();
        this.data.addAll(data);
        this.lie = lie;
        this.context = context;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fragment_two, container, false);
        init(component);
        return component;
    }

    public void init(Component component) {
        mLocales = Locale.getAvailableLocales();
        for (Locale mLocale : mLocales) {
            data.add(mLocale.toString().charAt(0) + "");
        }
        ListContainerByRes tlv = (ListContainerByRes) component.findComponentById(ResourceTable.Id_tlv);
        tlv.setFastScrollBg(new Color(Color.getIntColor("#BBBBBB")));
//        tlv.setScrollBarRes(ResourceTable.Media_icon);
        tlv.setScrollBarRes(ResourceTable.Graphic_bg_text);
        tlv.setScrollBarResHW(60, 250);
//        tlv.setTextBackGroudRes(ResourceTable.Media_icon);
        tlv.setTextBackGroudRes(ResourceTable.Graphic_left_bg1);
        tlv.setTextBackGroudShapeWH(200, 200);
        tlv.setTextColors(Color.RED);
        tlv.setLie(lie);
        tlv.setShowTextList(data);
        TableLayoutManager manager = new TableLayoutManager();
        manager.setColumnCount(lie);
        tlv.setLayoutManager(manager);
        TestListViewAdapter testListViewAdapter = new TestListViewAdapter(context, mLocales);
        testListViewAdapter.setmTabCount(lie);
        tlv.setItemProvider(testListViewAdapter);
    }
}