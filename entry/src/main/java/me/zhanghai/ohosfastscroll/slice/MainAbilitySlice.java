package me.zhanghai.ohosfastscroll.slice;


import me.zhanghai.ohosfastscroll.ResourceTable;
import me.zhanghai.ohosfastscroll.fragment.*;
import me.zhanghai.ohosfastscroll.left.LeftTouchComponent;
import me.zhanghai.ohosfastscroll.left.RotateComponent;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.RadioContainer;
import ohos.agp.utils.Color;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainAbilitySlice extends AbilitySlice {

    LeftTouchComponent leftTouchCompent;
    RotateComponent rotateComponent;
    RadioContainer radioContainer;
    List<Fraction> fractionList = new ArrayList<>();
    //数据

    public List<String> data = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        init();
    }

    public void init() {
        initData();
        leftTouchCompent = (LeftTouchComponent) findComponentById(ResourceTable.Id_cpt_lefttouch);
        rotateComponent = (RotateComponent) findComponentById(ResourceTable.Id_cpt_rotate);
        //设置左上角图标颜色，横线间隔距离，以及横线长度
        rotateComponent.setPainAndView(Color.BLACK, 20, 40);
        //设置左视图
        leftTouchCompent.setLeftView(ResourceTable.Layout_left);
        //关联滑动和旋转
        leftTouchCompent.setGO(rotateComponent);
        //查找页面控件
        leftTouchCompent.setIsSlide(false);
        Image but_change = (Image) findComponentById(ResourceTable.Id_but_change);
        but_change.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setUri(Uri.parse("https://developer.harmonyos.com/cn/docs/documentation/doc-references/button-0000001054358693"));
                startAbility(intent);
            }
        });
        //查找左边视图控件
        DirectionalLayout directionalLayout = (DirectionalLayout) leftTouchCompent.getLeftView(ResourceTable.Id_dl_left);
        directionalLayout.setAlpha(0.5f);
        directionalLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (leftTouchCompent.getRotate() > 175) {
                    leftTouchCompent.closeLeft();
                }
            }
        });
        radioContainer = (RadioContainer) leftTouchCompent.getLeftView(ResourceTable.Id_rc);
        radioContainer.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                changePage(i);
                if (leftTouchCompent.getRotate() > 175) {
                    leftTouchCompent.closeLeft();
                }
            }
        });
    }

    private void initData() {
        Locale[] mLocales = Locale.getAvailableLocales();
        for (Locale mLocale : mLocales) {
            data.add(mLocale.toString().charAt(0) + "");
        }
        FractionScheduler manager = ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler();
        fractionList.add(new Fragment_ListContanerByShape(this, mLocales, data, 1));
        fractionList.add(new FragmentGridListByShape(this, mLocales, data, 3));
        fractionList.add(new FragmentListContainerByRes(this, mLocales, data, 1));
        fractionList.add(new FragmentGridListByRes(this, mLocales, data, 3));
        fractionList.add(new FragmentScrollView());
        fractionList.add(new FragmentHorizontalScrollView());
        fractionList.add(new FragmentWebView());
        for (Fraction fraction : fractionList) {
            manager.add(ResourceTable.Id_test_fraction, fraction);
            manager.hide(fraction);
        }
        manager.show(fractionList.get(0));
        manager.submit();
    }

    public void changePage(int location) {
        hideAllFraction();
        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .show(fractionList.get(location))
                .submit();
    }

    private void hideAllFraction() {
        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .hide(fractionList.get(0))
                .hide(fractionList.get(1))
                .hide(fractionList.get(2))
                .hide(fractionList.get(3))
                .hide(fractionList.get(4))
                .hide(fractionList.get(5))
                .hide(fractionList.get(6))
                .submit();
    }
}
