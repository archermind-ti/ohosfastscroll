/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.zhanghai.ohosfastscroll.slice.provider;


import me.zhanghai.ohosfastscroll.ResourceTable;
import ohos.agp.components.*;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class TestListViewAdapter extends BaseItemProvider {
    List<Locale> conList = new ArrayList<>();
    Context context;
    private int mTabCount = 1, screenWidth;

    public TestListViewAdapter(Context context, Locale[] mLocales) {
        if (mLocales != null) {
            ArrayList<Locale> arrayList = new ArrayList<Locale>(Arrays.asList(mLocales.clone()));
            this.conList.addAll(arrayList);
        }
        this.context = context;
    }

    public void setmTabCount(int mTabCount) {
        this.mTabCount = mTabCount;
        screenWidth = DisplayManager.getInstance().getDefaultDisplay(context).get().getRealAttributes().width;
    }

    @Override
    public int getCount() {
        return conList == null ? 0 : conList.size();
    }

    @Override
    public Object getItem(int i) {
        return conList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        if (convertComponent == null) {
            convertComponent = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_sample, null, false);
            viewHolder = new ViewHolder();
            viewHolder.text = (Text) convertComponent.findComponentById(ResourceTable.Id_item_index);
            viewHolder.item_name = (Text) convertComponent.findComponentById(ResourceTable.Id_item_name);
            convertComponent.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertComponent.getTag();
        }
        viewHolder.text.setText(conList.get(position) + "");
        viewHolder.item_name.setText(conList.get(position).getDisplayName());
        if (mTabCount > 1) {
            int i = screenWidth / mTabCount - convertComponent.getMarginLeft() - convertComponent.getMarginRight();
            convertComponent.setWidth(i);
        }
        return convertComponent;
    }

    static class ViewHolder {
        Text text, item_name;
    }
}
