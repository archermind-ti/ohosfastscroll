/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.zhanghai.ohosfastscroll.left;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class RotateComponent extends Component implements Component.DrawTask{

    public float height,width,between,length;//控件宽高，横线之间间隔，横线长度
    Paint mDeafultPaint = new Paint();
    float rotate=0;
    float baseRote;
//    public int small = 3;
    public RotateComponent(Context context) {
        super(context);
        init();
    }

    public RotateComponent(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public RotateComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    public RotateComponent(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
        init();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.rotate(rotate,width/2,height/2);
        if (rotate<=180){
//        canvas.drawLine(width/2-length/2+rotate*length/360,between,width-(width-length)/2,between+rotate*between/180,mDeafultPaint);
//        canvas.drawLine(width/2-length/2,between*2,width-(width-length)/2,between*2,mDeafultPaint);
//        canvas.drawLine(width/2-length/2+rotate*length/360,between*3,width-(width-length)/2,between*3-rotate*between/180,mDeafultPaint);
            canvas.drawLine(width/2-length/2+rotate*length/360,height/2-between,width-(width-length)/2,height/2-between+rotate*between/180,mDeafultPaint);
            canvas.drawLine(width/2-length/2,height/2,width-(width-length)/2,height/2,mDeafultPaint);
            canvas.drawLine(width/2-length/2+rotate*length/360,height/2+between,width-(width-length)/2,height/2+between-rotate*between/180,mDeafultPaint);
        } else {
            canvas.drawLine(width/2-(length*(rotate-180))/360,height/2-between,width-(width-length)/2,height/2-(between*(rotate-180))/180,mDeafultPaint);
            canvas.drawLine(width/2-length/2,height/2,width-(width-length)/2,height/2,mDeafultPaint);
            canvas.drawLine(width/2-(length*(rotate-180))/360,height/2+between,width-(width-length)/2,height/2+(between*(rotate-180))/180,mDeafultPaint);
        }
//        canvas.drawLine(0,5,100,5,mDeafultPaint);
    }


    public void init(){
        addDrawTask(this);
        System.out.println("--------------------");
//        setTouchEventListener(this);
        mDeafultPaint.setAntiAlias(true);
        mDeafultPaint.setColor(Color.BLACK);
        mDeafultPaint.setStrokeWidth(8);
        mDeafultPaint.setStyle(Paint.Style.STROKE_STYLE);
        mDeafultPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
        height = this.getHeight();
        width = this.getWidth();
        between = 40;
        length = 90;
    }
    public void changeLocation(float rote,int x,int y,int zeroX,int zeroY){

    }
    public void setRotate(float rotate){
//        System.out.println(baseRote+"接收的rotate"+rotate);
        if (rotate<0){
            rotate=0;
        }
        if (rotate>180){
            rotate =180;
        }
        if (rotate==180){
            baseRote = 180;
        }
        if (rotate ==0){
            baseRote = 0;
        }
        if (baseRote==180) {
            this.rotate = baseRote + (180 - rotate);
            if (this.rotate==360){
                baseRote=0;
            }
        }
        if (baseRote==0){
            this.rotate = rotate;
        }
            invalidate();
    }
    public void setIsRoundLine(boolean f){
        mDeafultPaint.setStrokeCap(f?Paint.StrokeCap.ROUND_CAP:Paint.StrokeCap.SQUARE_CAP);
        mDeafultPaint.setColor(f?Color.GREEN:Color.BLACK);
        invalidate();
    }
    //画笔颜色，横线间距，横线长度
    public void setPainAndView(Color color,int between,int length){
        mDeafultPaint.setColor(color);
        this.between = between;
        this.length = length;
        invalidate();
    }
}
