/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.zhanghai.ohosfastscroll.left;

import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Timer;
import java.util.TimerTask;

public class LeftTouchComponent extends StackLayout implements Component.DrawTask, Component.TouchEventListener {

    Context context;
    int width, height;
    Component cpt;
    float screenX, moveX;
    float nowX;
    int resoureId;
    int selfRotate;
    Timer t = new Timer();
    RotateComponent go;
    boolean isSlide = true;

    public LeftTouchComponent(Context context) {
        super(context);
        initthis(context);
    }

    public LeftTouchComponent(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initthis(context);
    }

    public LeftTouchComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initthis(context);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (isSlide) {
            switch (touchEvent.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    loadLeft();
                    screenX = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getX();
                    nowX = cpt.getContentPositionX();
                    break;
                case TouchEvent.PRIMARY_POINT_UP:

                    break;
                case TouchEvent.POINT_MOVE:
                    moveX = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getX();
                    float num = nowX + moveX - screenX;
                    cpt.getContentPositionX();
                    if (num >= -width && num <= 0) {
                        if (cpt.getContentPositionX() <= 0 && cpt.getContentPositionX() >= -width) {
                            cpt.setContentPositionX(num);
                            go.setRotate((width + num) * 180 / width);
                        }
                    } else {
                        if (num > 0) {
                            cpt.setContentPositionX(0);
                            go.setRotate(180);
                            selfRotate = 180;
                            break;
                        } else {
                            cpt.setContentPositionX(-width);
                            go.setRotate(0);
                            selfRotate = 0;
                            break;
                        }
                    }
                    break;
                case TouchEvent.CANCEL:
                    break;
            }
        }
        return true;
    }

    public void initthis(Context context) {
        setTouchEventListener(this);
        this.context = context;
        height = getHeight();
        width = getWidth();
    }

    public void setLeftView(int resouresId) {
        this.resoureId = resouresId;
        cpt = LayoutScatter.getInstance(context).parse(resoureId, null, false);
    }

    public void loadLeft() {
        if (width == 0) {
            width = getWidth();
            height = getHeight();
//            cpt = LayoutScatter.getInstance(context).parse(resoureId, null, false);
//            dl_left = (DirectionalLayout) cpt.findComponentById(ResourceTable.Id_dl_left);
            this.addComponent(cpt);
            cpt.setContentPositionX(-width);
//            dl_left.setClickedListener(new ClickedListener() {
//                @Override
//                public void onClick(Component component) {
//                    if (selfRotate==180){
//                        closeLeft();
//                    }
//                }
//            });
        }
    }

    public void setGO(RotateComponent go) {
        this.go = go;
        go.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                loadLeft();
                go.setClickable(false);
                if (selfRotate == 0) {
                    openLeft();
                } else {
                    closeLeft();
                }
            }
        });
    }

    public void openLeft() {
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                selfRotate += 10;
                if (selfRotate <= 180) {
                    context.getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            if (selfRotate >= 180) {
//                                selfRotate = 0;
                                t.cancel();
                                go.setClickable(true);
                            }
                            go.setRotate(selfRotate);
                            if (selfRotate > 180) {//防止执行越界
                                selfRotate = 180;
                            }
                            int i = -(180 - selfRotate) * width / 180;
                            cpt.setContentPositionX(i);
//                            setleftAlph(dl_left,cpt.getContentPositionX());
//                            System.out.println(selfRotate);
                        }
                    });
                } else {
                    this.cancel();
                }
            }
        }, 0, 10);
    }

    public void closeLeft() {
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                selfRotate -= 10;
                if (selfRotate >= 0) {
                    context.getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            if (selfRotate <= 0) {
//                                selfRotate = 0;
                                t.cancel();
                                go.setClickable(true);
                            }
                            go.setRotate(selfRotate);
                            if (selfRotate < 0) {//防止执行越界
                                selfRotate = 0;
                            }
                            int i = -(180 - selfRotate) * width / 180;
                            cpt.setContentPositionX(i);
//                            System.out.println(selfRotate);
                        }
                    });
                } else {
                    this.cancel();
                }
            }
        }, 0, 10);
    }

    public Component getLeftView(int resoureId) {
        return cpt.findComponentById(resoureId);
    }

    public float getRotate() {
        return selfRotate;
    }

    public void setIsSlide(boolean isSlide) {
        this.isSlide = isSlide;
    }
}
