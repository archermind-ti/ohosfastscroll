# FastScrollSample

#### 项目介绍

FastScrollSample是一款完全可定制覆盖轨道，拇指，弹出，动画和滚动，易于使用的快速滚动的列表控件项目。

#### 安装教程

1. 将库添加到您的模块 build.gradle中：

```
allprojects{
    repositories{
        mavenCentral()
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation 'com.gitee.archermind-ti:fastscroll:1.0.0'
}
```

#### 功能演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0607/145129_974a8fcf_8856070.gif )
![输入图片说明](https://images.gitee.com/uploads/images/2021/0607/145145_4a390016_8856070.gif )
![输入图片说明](https://images.gitee.com/uploads/images/2021/0607/145201_1a073b30_8856070.gif)

#### 使用说明

   - 在布局文件中使用该控件
```java
    <me.panavtec.library.ListContainerByRes
        ohos:id="$+id:tlv"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:text="$string:navigation_recycler_view_list_stateful"
        ohos:text_alignment="center"
        ohos:text_size="50fp"/>
```

- 在fraction中使用该控件
```java
    public void init(Component component) {
          mLocales = Locale.getAvailableLocales();
          for (Locale mLocale : mLocales) {
              data.add(mLocale.toString().charAt(0) + "");
          }
          tlv = (ListContainerByRes) component.findComponentById(ResourceTable.Id_tlv);
          //设置滚动条的背景颜色
          tlv.setFastScrollBg(new Color(Color.getIntColor("#BBBBBB")));
          //设置滚动条的图像资源
          tlv.setScrollBarRes(ResourceTable.Graphic_bg_text);
          //设置滚动条的宽高
          tlv.setScrollBarResHW(60,250);
          //设置文字的背景图片
          tlv.setTextBackGroudRes(ResourceTable.Graphic_left_bg1);
          //设置背景图片的宽高
          tlv.setTextBackGroudShapeWH(200,200);
          //设置文字颜色
          tlv.setTextColorops(Color.RED);
          //设置列数（与listcontainer的列数相同，用于计算高度）
          tlv.setLie(lie);
          //设置显示的文字内容
          tlv.setShowTextList(data);
          //设置listcontainer列数
          TableLayoutManager manager = new TableLayoutManager();
          manager.setColumnCount(lie);
          tlv.setLayoutManager(manager);
          TableLayoutManager c = (TableLayoutManager) tlv.getLayoutManager();
          System.out.println(c.getColumnCount());
          tlv.setItemProvider(new TestListViewAdapter(context,mLocales));
      }
```

#### 版本迭代

- v1.1.7
- commit id c96504745ab54fa9da4bb9cdd5693139d58c5715


## License

    Copyright 2019 Google LLC

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        https://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


