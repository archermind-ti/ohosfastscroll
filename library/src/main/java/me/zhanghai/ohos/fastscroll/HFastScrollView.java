package me.zhanghai.ohos.fastscroll;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public class HFastScrollView extends ScrollView implements Component.ScrolledListener, Component.DrawTask, Component.EstimateSizeListener, Component.TouchEventListener {

    HViewHelp viewHelp = new HViewHelp(this, false);

    public HFastScrollView(Context context) {
        super(context);
        init();
    }

    public HFastScrollView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public HFastScrollView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
//        viewHelp.onDraw(component,canvas);
//        viewHelp.onDraw(component,canvas);
    }

    public void init() {
        setTouchEventListener(this);
        setEstimateSizeListener(this);
        setScrolledListener(this);
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        return false;
    }

    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
        viewHelp.updateTextlocation(i, i1, i2, i3);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
//        System.out.println("是否回弹"+getReboundEffect());
        viewHelp.addFastView();
        viewHelp.onTouchEvent(component, touchEvent);
        return true;
    }

}
