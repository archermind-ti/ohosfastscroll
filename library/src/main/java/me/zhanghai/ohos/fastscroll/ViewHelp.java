package me.zhanghai.ohos.fastscroll;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.multimodalinput.event.TouchEvent;

public class ViewHelp implements Component.DrawTask, Component.TouchEventListener {
    boolean isInOneStep = true;//是否是同一操作内
    ComponentContainer container;
    Text text;
    Paint paint = new Paint();
    AnimatorProperty animatorProperty;
    AnimatorProperty animatorPropertx;
    StackLayout directionalLayout;
    int bigHight, smallHight;
    float screenRightY = 0;
    float x, y;
    float oldPosition;//text的起始位置
    public static final double minBetwenSize = 0.0000001;//防止快速滑动回弹事件
    boolean isLeft;

    public ViewHelp(ComponentContainer container, boolean isLeft) {
        super();
        this.container = container;
        this.isLeft = isLeft;
        init();
//        addFastView();
    }

    private void init() {
        container.addDrawTask(this);
        paint.setColor(Color.YELLOW);
        paint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

//        canvas.drawCircle(new Circle(50,50,60),paint);

    }

    public void addFastView() {
        if (text == null) {
            initAddView(container);
        }
        startAnimal();
    }

    public void updateTextLocation(int i, int i1, int i2, int i3) {
        if (!isinSmall(x, y)) {
            int i4 = i1 * smallHight / bigHight;
            text.setContentPositionY(i4);
        }

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {

        System.out.println("点击的按钮坐标和高度====x" + x + "=y=" + y + "=left=" + directionalLayout.getLeft() + "=right=" + directionalLayout.getRight());
        System.out.println("=top=" + directionalLayout.getTop() + "=bottom=" + directionalLayout.getBottom());
//        System.out.println(bigHight+"===控件的点击事件"+touchEvent.getAction()+"===="+text.getHeight()+"=="+container.getHeight()+"=="+container.getComponentAt(0).getHeight());
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                x = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
                y = touchEvent.getPointerPosition(touchEvent.getIndex()).getY();
                screenRightY = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY();
                oldPosition = text.getContentPositionY();
                if (isinSmall(x, y)) {
//                        container.setDraggedListener(1, this);
                    doPosition(x, y);
                    System.out.println("控件内按下");
                } else {
                    System.out.println("TestScroolVIew按下事件");
//                        addFastView();
//                        container.setDraggedListener(1,null);
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                System.out.println("抬起事件");
                endAnimal();
                break;
            case TouchEvent.POINT_MOVE:
                if (animatorPropertx != null && animatorPropertx.isRunning()) {
                    animatorPropertx.cancel();
                    isInOneStep = true;
                }
                if (isinSmall(x, y)) {
                    double xnowY = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY();
                    double position = oldPosition + xnowY - screenRightY;
                    if (position > 0 && position < smallHight) {
                        text.setContentPositionY((float) position);
                        container.scrollTo(0, (int) (text.getContentPositionY() * bigHight / smallHight));
                    } else {//滑动越界处理
                        position = (position > 0 ? (smallHight - minBetwenSize) : (0 + minBetwenSize));//防止快速滑动回弹事件
                        text.setContentPositionY((float) position);
                        container.scrollTo(0, (int) (text.getContentPositionY() * bigHight / smallHight));
                    }
                } else {
                    addFastView();
                }
                break;
            case TouchEvent.CANCEL:
                System.out.println("TestScroolVIewCancel事件");
                break;
        }

        return true;
    }

    public void startAnimal() {
        if (isLeft) {
            if (isInOneStep && directionalLayout.getContentPositionX() == -directionalLayout.getWidth()) {
                isInOneStep = false;
                animatorProperty = directionalLayout.createAnimatorProperty();
                System.out.println("开始动画================startAnimal");
                animatorProperty.moveFromX(-directionalLayout.getWidth()).moveToX(0).setDuration(2).setDelay(0);
                animatorProperty.start();
            }
        } else {
            if (isInOneStep && directionalLayout.getContentPositionX() == container.getWidth()) {
                isInOneStep = false;
                animatorProperty = directionalLayout.createAnimatorProperty();
                System.out.println("开始动画================startAnimal");
                animatorProperty.moveFromX(container.getWidth()).moveToX(container.getWidth() - directionalLayout.getWidth()).setDuration(2).setDelay(0);
                animatorProperty.start();
            }
        }

    }

    public void endAnimal() {
        if (isLeft) {
            System.out.println(directionalLayout.getWidth() + "endAnimal=========" + directionalLayout.getContentPositionX());
            if (0 == directionalLayout.getContentPositionX()) {
                animatorPropertx = directionalLayout.createAnimatorProperty();
                animatorPropertx.moveFromX(0).moveToX(-directionalLayout.getWidth()).setDuration(500).setDelay(1500);
                animatorPropertx.start();
                isInOneStep = true;
            }
        } else {
            System.out.println(directionalLayout.getWidth() + "endAnimal=========" + directionalLayout.getContentPositionX());
            if (container.getWidth() - directionalLayout.getWidth() == directionalLayout.getContentPositionX()) {
                animatorPropertx = directionalLayout.createAnimatorProperty();
                animatorPropertx.moveFromX(container.getWidth() - directionalLayout.getWidth()).moveToX(container.getWidth()).setDuration(500).setDelay(1500);
                animatorPropertx.start();
                isInOneStep = true;
            }
        }
    }

    public void initAddView(ComponentContainer container) {
        bigHight = container.getComponentAt(0).getHeight() - container.getHeight();
        directionalLayout = new StackLayout(container.getContext());
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig();
        layoutConfig.width = 50;
        layoutConfig.height = container.getHeight();
        if (!isLeft) {
            layoutConfig.setMarginLeft(container.getWidth() - directionalLayout.getWidth());
        }
        directionalLayout.setBackground(new PixelMapElement(CUtils.getPixelMap(container.getContext(), ResourceTable.Graphic_bg_gray)));
        directionalLayout.setLayoutConfig(layoutConfig);

        text = new Text(container.getContext());
        text.setTextSize(50);
        text.setBackground(new PixelMapElement(CUtils.getPixelMap(container.getContext(), ResourceTable.Graphic_bg_text)));
        text.setContentPositionY(0);
        text.setLayoutConfig(new StackLayout.LayoutConfig(50, 270));
        smallHight = container.getHeight() - text.getHeight();
        text.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                System.out.println("按到我了");
            }
        });
        directionalLayout.addComponent(text);
        container.addComponent(directionalLayout);
        //
    }

    public boolean isinSmall(float x, float y) {
        if (isLeft) {
            if (directionalLayout.getLeft() < x && x < directionalLayout.getWidth() && directionalLayout.getTop() < y && y < directionalLayout.getHeight()) {
                System.out.println("我在左边快捷滑动里面");
                return true;
            }
        } else {
            if (directionalLayout.getLeft() - directionalLayout.getWidth() < x && x < directionalLayout.getRight() - directionalLayout.getWidth() && directionalLayout.getTop() < y && y < directionalLayout.getHeight()) {
                System.out.println("我在右边快捷滑动里面");
                return true;
            }
        }
        System.out.println("我没在快捷滑动里面");
        return false;
    }

    public void doPosition(float x, float y) {
        if (y > smallHight) {
            y = smallHight;
        }
        int resultY = (int) (y * (bigHight) / smallHight);
        text.setContentPositionY(y);
        container.scrollTo(0, resultY);
    }

    public void isLeft() {

    }
}
