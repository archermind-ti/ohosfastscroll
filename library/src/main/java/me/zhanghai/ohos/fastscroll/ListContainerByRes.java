package me.zhanghai.ohos.fastscroll;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ListContainerByRes extends ListContainer implements Component.ScrolledListener,
        Component.DrawTask, Component.EstimateSizeListener, Component.TouchEventListener {

    Paint paint = new Paint();//画图像
    Paint paint2 = new Paint();//画长条
    Color lastBgColor = Color.RED;
    int allHeight;
    float nowx, nowY;
    float rightPointLocation;
    float moveStart;
    float screenRightY = 10;
    float lastPosition = 0;
    int bigHight, smallHight;
    float itemHight;
    String showText;
    boolean isOnAnimal = false;
    int statrPos;
    PixelMap pixelMap;
    int pixWidth, pixheight;
    PixelMapHolder pixelMapHolder;
    float viewx, viewx2;
    Timer timer = new Timer();
    PixelMap textBackPix;
    PixelMapHolder textBackPixHolder;
    int textBackWidth, textBackpixheight;
    Color texeColor;
    boolean isShowText;//是否显示图标
    int lie;
    List<String> strList;

    public ListContainerByRes(Context context) {
        super(context);
        init();
    }

    public ListContainerByRes(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public ListContainerByRes(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
//        System.out.println(viewx+"="+rightPointLocation+"="+this.getWidth()+"="+this.getHeight());
        if (isOnAnimal) {
            rightToShow();
            canvas.drawRect(new RectFloat(viewx, 0, this.getWidth(), this.getHeight()), paint2, lastBgColor);
            canvas.drawPixelMapHolderRect(pixelMapHolder, new RectFloat(viewx, rightPointLocation, viewx + pixWidth, pixheight + rightPointLocation), paint);
            drawText(canvas);
        } else {
            canvas.drawRect(new RectFloat(viewx, 0, this.getWidth(), this.getHeight()), paint2, lastBgColor);
            canvas.drawPixelMapHolderRect(pixelMapHolder, new RectFloat(viewx, rightPointLocation, viewx + pixWidth, pixheight + rightPointLocation), paint);
            drawText(canvas);
        }
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        return false;
    }

    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
        if (!isinRight(nowx, nowY)) {
            getIntLocation(i1);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                isOnAnimal = false;
                cancelAnimal();
                viewx = this.getWidth();
                showText = "";
                moveStart = 0;
                nowx = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
                nowY = touchEvent.getPointerPosition(touchEvent.getIndex()).getY();
                screenRightY = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY();
//                System.out.println("点击的位置x："+isinRight(nowx,nowY));
                getAllHight();
                if (isinRight(nowx, nowY)) {
                    isOnAnimal = false;
                    viewx = this.getWidth() - pixWidth;
                    viewx2 = this.getWidth() - pixWidth - 100;
                    rightPointLocation = nowY;
                    cancelAnimal();
                    statrPos = this.getWidth() - pixWidth;
                    setIntLocation(nowY);
                    invalidate();
                    lastPosition = rightPointLocation;
                }
                invalidate();
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                isShowText = false;
                isOnAnimal = true;
                invalidate();
                break;
            case TouchEvent.POINT_MOVE:
                viewx = this.getWidth() - pixWidth;
                viewx2 = this.getWidth() - pixWidth - 100;
                cancelAnimal();
                isOnAnimal = false;
                statrPos = this.getWidth() - pixWidth;
                float xnowY = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY();
                if (isinRight(nowx, nowY)) {
                    isShowText = true;
                    if (lastPosition + xnowY - screenRightY >= 0 && lastPosition + xnowY - screenRightY <= smallHight) {
                        rightPointLocation = lastPosition + xnowY - screenRightY;
                        invalidate();
                    } else {
                        rightPointLocation = lastPosition + xnowY - screenRightY <= 0 ? 0 : smallHight;
                    }
                    setIntLocation(rightPointLocation);
                    if (rightPointLocation <= 0 || rightPointLocation >= smallHight) {
                        invalidate();
                    }
                    return true;
                }
                invalidate();
                break;
        }
        return true;
    }

    private boolean isinRight(float x, float y) {
        if (this.getWidth() - pixWidth < x && x < this.getWidth() && 0 < y && y < this.getHeight()) {
            return true;
        }
        return false;
    }

//    public void doRight(float x, float y) {
//        int resultY = (int) (y * bigHight / smallHight);
//        rightPointLocation = y;
//    }

    private void getAllHight() {
        if (allHeight == 0) {
            itemHight = this.getComponentAt(0).getHeight();
            allHeight = this.getComponentAt(0).getHeight() * (getChildCount() % lie == 0 ? getChildCount() / lie : getChildCount() / lie + 1);
            bigHight = allHeight - getHeight();
            smallHight = getHeight() - pixheight;
            System.out.println(getChildCount() + "这是总高度" + allHeight + "=item高度" + itemHight + "=bighight" + bigHight + "=small" + smallHight);
        }
    }

    private void init() {
        addDrawTask(this);
        setTouchEventListener(this);
        setEstimateSizeListener(this);
        setScrolledListener(this);
        paint.setTextSize(70);
        paint.setColor(Color.RED);
        paint.setAntiAlias(true);
        paint2.setTextSize(70);
        paint2.setColor(Color.GREEN);
        paint2.setAntiAlias(true);
//        pixelMap = CUtils.getPixelMap(this.getContext(), ResourceTable.Media_icon);
//        System.out.println(pixelMap.getImageInfo().size.height+"这是加载的图片>>>>>>>>>>>>>"+pixelMap.getImageInfo().size.width);
//        pixWidth = pixelMap.getImageInfo().size.width;
//        pixheight = pixelMap.getImageInfo().size.height;
        pixWidth = 50;
        pixheight = 200;
        statrPos = this.getWidth() - pixWidth;
//        pixelMapHolder = new PixelMapHolder(pixelMap);
        viewx = 1300;
        viewx2 = 1300 - 100;
    }

    //通过左视图设置右视图
    private float getIntLocation(float lastPosition) {
        float result;
        float count = itemHight;
        result = lastPosition / count;
        rightPointLocation = lastPosition * smallHight / bigHight;
        invalidate();
        return result;
    }

    //通过右侧视图控制左视图
    private int setIntLocation(float lastPosition) {
        if (lastPosition > smallHight) {
            lastPosition = smallHight;
            rightPointLocation = smallHight;
        }
        int result;
//        float count = itemHight;
        int num = (int) (lastPosition * bigHight / (smallHight * itemHight));
        result = (lastPosition * bigHight % (smallHight * itemHight) > 0 ? num + 1 : num) * lie;
        if (result >= getChildCount()) {//列表不满，防溢出
            result = getChildCount() - 1;
        }
        showText = strList.get(result);
        this.scrollTo(result);
        return result;
    }

    private synchronized void rightToShow() {
        timer = new Timer();
        int w = this.getWidth();
        showText = "";
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (statrPos <= w + 50) {
                    viewx = statrPos;
                    statrPos += 30;
                    getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {

                            invalidate();
                        }
                    });
                }
            }
        };
        timer.schedule(task, 1500);
    }

    private synchronized void cancelAnimal() {
        if (!isOnAnimal) {
            if (timer != null) {
                timer.cancel();
            }
        }
    }

    public void setFastScrollBg(Color lastBgColor) {
        this.lastBgColor = lastBgColor;
        invalidate();
    }

    public void setScrollBarRes(int resourceId) {
        pixelMap = CUtils.getPixelMap(getContext(), resourceId);
        pixWidth = pixelMap.getImageInfo().size.width;
        pixheight = pixelMap.getImageInfo().size.height;
        statrPos = this.getWidth() - pixWidth;
        pixelMapHolder = new PixelMapHolder(pixelMap);
        smallHight = this.getHeight() - pixheight;
    }

    public void setScrollBarResHW(int pixWidth, int pixheight) {
        this.pixWidth = pixWidth;
        this.pixheight = pixheight;
        statrPos = this.getWidth() - pixWidth;
        smallHight = this.getHeight() - pixheight;
    }

    public void setTextBackGroudRes(int resourceId) {
        textBackPix = CUtils.getPixelMap(getContext(), resourceId);
        textBackPixHolder = new PixelMapHolder(textBackPix);
        textBackWidth = textBackPix.getImageInfo().size.width;
        textBackpixheight = textBackPix.getImageInfo().size.height;
    }

    public void setTextBackGroudShapeWH(int width, int height) {
        textBackWidth = width;
        textBackpixheight = height;
    }

    public void setTextColors(Color color) {
        this.texeColor = color;
    }

    private void drawText(Canvas canvas) {
        if (texeColor != null) {
            paint.setColor(texeColor);
        }
        if (isShowText) {
            if (textBackWidth > 0) {
                canvas.drawPixelMapHolderRect(textBackPixHolder, new RectFloat(viewx2 - textBackWidth, rightPointLocation, viewx2 + 100, rightPointLocation + textBackpixheight), new Paint());
            }
            canvas.drawText(paint, showText, (float) (viewx2 - textBackWidth * 0.5), (float) (rightPointLocation + textBackpixheight * 0.5));
        }
    }

    public void setLie(int lie) {
        this.lie = lie;
    }

    public void setShowTextList(List<String> strList) {
        this.strList = strList;
    }
}
