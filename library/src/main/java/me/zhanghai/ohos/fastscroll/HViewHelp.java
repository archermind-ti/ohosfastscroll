package me.zhanghai.ohos.fastscroll;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.multimodalinput.event.TouchEvent;

public class HViewHelp implements Component.DrawTask, Component.TouchEventListener {

    private boolean isInOneStep = true;//是否是同一操作内
    private ComponentContainer container;
    private Text text;
    private Paint paint = new Paint();
    private AnimatorProperty animatorPropertx;
    private StackLayout directionalLayout;
    private int bigWidth, smallwidth;
    private float screenRightX = 0;
    private float x, y;
    private float oldPosition;//text的起始位置
    private static final double minBetwenSize =  0.0000001;//防止快速滑动回弹事件
    private boolean istop;

    public HViewHelp(ComponentContainer container, boolean istop) {
        super();
        this.container = container;
        this.istop = istop;
        init();
//        addFastView();
    }

    private void init() {
        container.addDrawTask(this);
        paint.setColor(Color.YELLOW);
        paint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

//        canvas.drawCircle(new Circle(50,50,60),paint);

    }

    public void addFastView() {
        if (text == null) {
            initAddView(container);
        }
        startAnimal();
    }

    public void updateTextlocation(int i, int i1, int i2, int i3) {
        if (!isinSmall(x, y)) {
            System.out.println(i + "=" + i1 + "==" + i2 + "===" + i3);
            int i4 = i * smallwidth / bigWidth;
            text.setContentPositionX(i4);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {

//        System.out.println("点击的按钮坐标和高度====x"+x+"=y="+y+"=left="+directionalLayout.getLeft()+"=right="+directionalLayout.getRight());
//        System.out.println("=top="+directionalLayout.getTop()+"=bottom="+directionalLayout.getBottom());
//        System.out.println(bigHight+"===控件的点击事件"+touchEvent.getAction()+"===="+text.getHeight()+"=="+container.getHeight()+"=="+container.getComponentAt(0).getHeight());
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (animatorPropertx != null && animatorPropertx.isRunning()) {
                    animatorPropertx.cancel();
                    isInOneStep = true;
                }
                x = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
                y = touchEvent.getPointerPosition(touchEvent.getIndex()).getY();
                screenRightX = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getX();
                oldPosition = text.getContentPositionX();
                startAnimal();
                if (isinSmall(x, y)) {
//                        container.setDraggedListener(1, this);
                    System.out.println("控件内按下");
                } else {
                    System.out.println("TestScroolVIew按下事件");
//                        addFastView();
//                        container.setDraggedListener(1,null);
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                System.out.println("抬起事件");
                endAnimal();
                break;
            case TouchEvent.POINT_MOVE:
                if (animatorPropertx != null && animatorPropertx.isRunning()) {
                    animatorPropertx.cancel();
                    isInOneStep = true;
                }
                if (isinSmall(x, y)) {
                    double xnowX = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getX();
                    double position = oldPosition + xnowX - screenRightX;
                    if (position > 0 && position < smallwidth) {
                        text.setContentPositionX((float) position);
                        container.scrollTo((int) (text.getContentPositionX() * bigWidth / smallwidth), 0);
                    } else {//滑动越界处理
                        position = position > 0 ? (smallwidth - minBetwenSize) : minBetwenSize;//防止快速滑动回弹事件
                        text.setContentPositionX((float) position);
                        container.scrollTo((int) (text.getContentPositionX() * bigWidth / smallwidth), 0);
                    }
                } else {
                    System.out.println("mei======================");
                    addFastView();
                }
                break;
            case TouchEvent.CANCEL:
                System.out.println("TestScroolVIewCancel事件");
                break;
        }

        return true;
    }

    private void startAnimal() {
        directionalLayout.setPosition(0, container.getHeight() - directionalLayout.getHeight());
//                if (animatorPropertx!=null){
//                animatorPropertx.cancel();}
//                isInOneStep = false;
//                animatorProperty = directionalLayout.createAnimatorProperty();
//                System.out.println("开始动画================startAnimal");
//                animatorProperty.moveFromY(container.getHeight()).moveToY(directionalLayout.getHeight()).setDuration(2).setDelay(0);
    }

    private void endAnimal() {
//            System.out.println(directionalLayout.getWidth()+"endAnimal========="+directionalLayout.getContentPositionX());
//            if (container.getWidth()-directionalLayout.getWidth()==directionalLayout.getContentPositionX()) {
        animatorPropertx = directionalLayout.createAnimatorProperty();
        animatorPropertx.moveFromY(container.getHeight() - directionalLayout.getHeight()).moveToY(container.getHeight()).setDuration(500).setDelay(0);
        animatorPropertx.start();
        isInOneStep = true;
//            }
    }

    private void initAddView(ComponentContainer container) {
        System.out.println("初始化的高度>>>>>>>>>>>>>.." + container.getHeight());
        bigWidth = container.getComponentAt(0).getWidth() - container.getWidth();
        directionalLayout = new StackLayout(container.getContext());
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig();
        layoutConfig.width = container.getWidth();
        layoutConfig.height = 100;
        if (!istop) {
            layoutConfig.setMarginTop(container.getHeight() - 100);
        }
        directionalLayout.setBackground(new PixelMapElement(CUtils.getPixelMap(container.getContext(), ResourceTable.Graphic_bg_gray)));
        directionalLayout.setLayoutConfig(layoutConfig);

        text = new Text(container.getContext());
        text.setTextSize(50);
        text.setBackground(new PixelMapElement(CUtils.getPixelMap(container.getContext(), ResourceTable.Graphic_bg_text)));
        text.setContentPositionX(0);
        text.setLayoutConfig(new StackLayout.LayoutConfig(270, 100));
        smallwidth = container.getWidth() - text.getWidth();
        text.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                System.out.println("按到我了");
            }
        });
        directionalLayout.addComponent(text);
        container.addComponent(directionalLayout);
        //
    }

    private boolean isinSmall(float x, float y) {
        if (istop) {
            if (directionalLayout.getLeft() < x && x < directionalLayout.getRight() && directionalLayout.getTop() < y && y < directionalLayout.getBottom()) {
                System.out.println("我在上快捷滑动里面");
                return true;
            }
        } else {
            if (directionalLayout.getLeft() < x && x < directionalLayout.getRight() && directionalLayout.getTop() < y && y < directionalLayout.getBottom()) {
                System.out.println("我在下快捷滑动里面");
                return true;
            }
        }
        return false;
    }

    public void doPosition(float x, float y) {
        if (x > smallwidth) {
            x = smallwidth;
        }
        int resultX = (int) (x * (bigWidth) / smallwidth);
        text.setContentPositionX(x);
        container.scrollTo(resultX, (int) 0);
    }
}
