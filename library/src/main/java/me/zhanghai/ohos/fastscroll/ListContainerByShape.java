package me.zhanghai.ohos.fastscroll;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ListContainerByShape extends ListContainer implements Component.ScrolledListener
        , Component.DrawTask
        , Component.EstimateSizeListener
        , Component.TouchEventListener {

    private Paint paint = new Paint();//画图像
    //  private Paint paint2 = new Paint();//画长条
    private Color lastBgColor;
    private int allHeight;
    private float nowx, nowY;
    private float rightPointLocation;
    private float screenRightY = 10;
    private float lastPosition = 0;
    private int bigHight, smallHight;
    private float itemHight;
    private String showText;
    private boolean isOnAnimal = false;
    private int startPos;
    private int pixWidth, pixHeight;
    private float viewx, viewx2;
    private Timer timer = new Timer();
    private int textBackWidth, textBackpixheight;
    private Color texeColor = Color.WHITE;
    private boolean isShowText;//是否显示图标
    private int lie;
    private List<String> strList;
    private ShapeElement background, thumb, thumbs;
    private int radius = 0;

    public ListContainerByShape(Context context) {
        this(context, null);
    }

    public ListContainerByShape(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public ListContainerByShape(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (isOnAnimal) {
            rightToShow();
        }
        drawShapeFile(canvas);
        drawText(canvas);
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        return false;
    }

    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
        if (isInRight(nowx, nowY)) {
            System.out.println(isInRight(nowx, nowY) + "_________________");
        } else {
            getIntLocation(i1);
        }
    }


    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                isOnAnimal = false;
                cancelAnimal();
                viewx = this.getWidth();
                showText = "";
                nowx = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
                nowY = touchEvent.getPointerPosition(touchEvent.getIndex()).getY();
                screenRightY = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY();
//                System.out.println("点击的位置x："+isinRight(nowx,nowY));
                getAllHight();
                if (isInRight(nowx, nowY)) {
                    isOnAnimal = false;
                    viewx = this.getWidth() - pixWidth;
                    viewx2 = this.getWidth() - pixWidth - 100;
                    rightPointLocation = nowY;
                    cancelAnimal();
                    startPos = this.getWidth() - pixWidth;
                    setIntLocation(nowY);
                    invalidate();
                    lastPosition = rightPointLocation;
                }
                invalidate();
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                System.out.println("没执行？");
                isShowText = false;
                isOnAnimal = true;
//                rightToShow();
                invalidate();
                break;
            case TouchEvent.POINT_MOVE:
                viewx = this.getWidth() - pixWidth;
                viewx2 = this.getWidth() - pixWidth - 100;
                cancelAnimal();
                isOnAnimal = false;
                startPos = this.getWidth() - pixWidth;
                float xnowY = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY();
                if (isInRight(nowx, nowY)) {
                    isShowText = true;
                    if (lastPosition + xnowY - screenRightY >= 0 && lastPosition + xnowY - screenRightY <= smallHight) {
                        rightPointLocation = lastPosition + xnowY - screenRightY;
                        invalidate();
                    } else {
                        rightPointLocation = lastPosition + xnowY - screenRightY <= 0 ? 0 : smallHight;
                    }
                    setIntLocation(rightPointLocation);
                    if (rightPointLocation <= 0 || rightPointLocation >= smallHight) {
                        invalidate();
                    }
                    return true;
                } else {
                    isShowText = false;
                }
                invalidate();
                break;
        }
        return true;
    }

    public boolean isInRight(float x, float y) {
        return this.getWidth() - pixWidth < x && x < this.getWidth() && 0 < y && y < this.getHeight();
    }

//    public void doRight(float x, float y) {
//        int resultY = (int) (y * bigHight / smallHight);
//        rightPointLocation = y;
//    }

    public void getAllHight() {
        if (allHeight == 0) {
            //滑动的距离3693.0  21这是总高度10710=item高度510.0=bighight8793=small1566
            //滑动的距离8793.0  21这是总高度10710=item高度510.0=bighight8793=small1566
            itemHight = this.getComponentAt(0).getHeight();
//            allHeight = this.getComponentAt(0).getHeight() * getChildCount();
            allHeight = this.getComponentAt(0).getHeight() * (getChildCount() % lie == 0 ? getChildCount() / lie : getChildCount() / lie + 1);
            bigHight = allHeight - getHeight();
//            smallHight = getHeight()-pixelMap.getImageInfo().size.height;
            smallHight = getHeight() - 200;
            System.out.println(getChildCount() + "这是总高度" + allHeight + "=item高度" + itemHight + "=bighight" + bigHight + "=small" + smallHight);
        }
    }

    public void init() {
        addDrawTask(this);
        setTouchEventListener(this);
        setEstimateSizeListener(this);
        setScrolledListener(this);
        paint.setTextSize(70);
        paint.setColor(Color.RED);
        paint.setAntiAlias(true);
//        paint2.setTextSize(70);
//        paint2.setColor(Color.GREEN);
//        paint2.setAntiAlias(true);
        pixWidth = 50;
        pixHeight = 200;
        startPos = this.getWidth() - pixWidth;
        viewx = 1300;
        viewx2 = 1300 - 100;
    }


//    private boolean mIsScrolling = false;
//
//    @Override
//    public void scrolledStageUpdate(Component component, int newStage) {
//        mIsScrolling = newStage != 0;
//    }


    //通过左视图设置右视图
    public float getIntLocation(float lastPosition) {
        float count = itemHight;
        float result = lastPosition / count;
        rightPointLocation = lastPosition * smallHight / bigHight;
        invalidate();
        return result;
    }

    //通过右侧视图控制左视图
    public int setIntLocation(float lastPosition) {
        if (lastPosition > smallHight) {
            lastPosition = smallHight;
            rightPointLocation = smallHight;
        }
        int result;
//        float count = itemHight;
        int num = (int) (lastPosition * bigHight / (smallHight * itemHight));
        result = (lastPosition * bigHight % (smallHight * itemHight) > 0 ? num + 1 : num) * lie;
        if (result >= getChildCount()) {//列表不满，防溢出
            result = getChildCount() - 1;
        }
        showText = strList.get(result);
        this.scrollTo(result);
        return result;
    }

    public synchronized void rightToShow() {
        timer = new Timer();
        int w = this.getWidth();
        showText = "";
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (startPos <= w + 50) {
                    viewx = startPos;
                    startPos += 30;
                    getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {

                            invalidate();
                        }
                    });
                }
            }
        };
        timer.schedule(task, 1500);
    }

    public synchronized void cancelAnimal() {
        if (!isOnAnimal) {
            if (timer != null) {
                timer.cancel();
            }
        }
    }

    public void setFastScrollBg(Color lastBgColor) {
        this.lastBgColor = lastBgColor;
        invalidate();
    }

    public void setScrollBarWH(int pixWidth, int pixheight) {

        this.pixWidth = pixWidth;
        this.pixHeight = pixheight;
        startPos = this.getWidth() - pixWidth;
        invalidate();
    }

    public void setTextBackGroudShapeWH(int width, int height) {
        textBackWidth = width;
        textBackpixheight = height;
    }

    public void setTextColorops(Color color) {
        this.texeColor = color;
    }

    private void drawText(Canvas canvas) {
        if (texeColor != null) {
            paint.setColor(texeColor);
        }
        if (isShowText) {
            if (textBackWidth > 0) {
                if (thumbs == null) {
                    thumbs = new ShapeElement(getContext(), ResourceTable.Graphic_afs_popup_background_show);
                }
//                thumbs.setCornerRadius(radius);
                thumbs.setCornerRadiiArray(new float[]{0, 44, 44, 0});
                thumbs.setBounds((int) viewx2 - textBackWidth, (int) rightPointLocation, (int) viewx2 + textBackWidth, (int) rightPointLocation + 50 + textBackpixheight);
            }
            thumbs.drawToCanvas(canvas);
            canvas.drawText(paint, showText, (float) (viewx2 - textBackWidth * 0.5), rightPointLocation + textBackpixheight);
        }
    }

    public void setLie(int lie) {
        this.lie = lie;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void setShowTextList(List<String> strList) {
        this.strList = strList;
    }

    private void drawShapeFile(Canvas canvas) {
        if (background == null) {
            background = new ShapeElement(getContext(), ResourceTable.Graphic_afs_popup_background);
        }
        background.setCornerRadius(radius);
        background.setBounds((int) viewx, 0, this.getWidth(), this.getHeight());
        background.drawToCanvas(canvas);
        if (thumb == null) {
            thumb = new ShapeElement(getContext(), ResourceTable.Graphic_afs_md2_thumb);
        }
        thumb.setCornerRadius(radius);
        thumb.setBounds((int) viewx, (int) rightPointLocation, (int) viewx + pixWidth, (int) (pixHeight + rightPointLocation));
        thumb.drawToCanvas(canvas);
    }


}
